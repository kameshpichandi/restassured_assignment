package apitesting;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class TsetingGetRequest {

	@Test(priority = 1)

	public void getMethod() {

		Response response = RestAssured.get("https://reqres.in/api/users?page=2");
		int statusCode = response.getStatusCode();
		System.out.println(statusCode);
		System.out.println(response.getBody().asString());

	}

	@Test(priority = 2)

	public void assertGetMethod() {

		Response response = RestAssured.get("https://reqres.in/api/users?page=2");
		int statusCode = response.getStatusCode();
		Assert.assertEquals(200, statusCode);

	}

}
