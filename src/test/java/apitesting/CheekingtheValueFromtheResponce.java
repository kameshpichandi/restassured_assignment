package apitesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class CheekingtheValueFromtheResponce {

	@Test(priority = 2)
	public void cheekingValue() {
		baseURI = "https://reqres.in/";
		given().get("/api/users?page=2").then().body("data.first_name", hasItem("Lindsay"));

	}

	@Test(priority = 1)

	public void cheekingMultipleValue() {
		baseURI = "https://reqres.in/";
		given().get("/api/users?page=2").then().body("data.last_name", hasItems("Howell", "Ferguson"));

	}

}
