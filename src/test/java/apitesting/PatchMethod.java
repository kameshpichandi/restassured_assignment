package apitesting;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class PatchMethod {

	@Test

	public void updatingDataUsingPut() {

		JSONObject request = new JSONObject();
		request.put("name", "Ramesh Ramajayam");
		request.put("job", "Electronics Engineer");
		// System.out.println(request);

		given().body(request.toJSONString()).patch("https://reqres.in/api/users/2").then().statusCode(200)
				.body("updatedAt", greaterThanOrEqualTo("2023-01-24T10:04:44.028Z"));

		
	}

}
