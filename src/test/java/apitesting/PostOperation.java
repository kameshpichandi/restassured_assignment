package apitesting;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class PostOperation {

	 @Test

	public void postOperationTesting() {

		JSONObject request = new JSONObject();
		request.put("name", "charumathi");
		request.put("job", "developer");
		System.out.println(request);

		baseURI = "https://reqres.in/api";
		given().body(request.toJSONString()).when().post("/users").then().statusCode(201);
	}

}
