package apitesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class TestingGetRequestusingGivenWhenApproach {

	@Test(priority = 1)

	public void testStatusCode() {
		baseURI = "https://reqres.in";
		given().get("/api/users?page=2").then().statusCode(200);
	}

	@Test(priority = 2)

	public void testParticularValue() {
		baseURI = "https://reqres.in";
		given().get("/api/users?page=2").then().body("data[1].email", equalTo("lindsay.ferguson@reqres.in"));

	}

	@Test(priority = 3)
	public void printAllValue() {
		baseURI = "https://reqres.in";
		given().get("/api/users?page=2").then().log().all();

	}

}
