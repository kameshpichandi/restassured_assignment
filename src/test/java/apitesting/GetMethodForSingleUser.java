package apitesting;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.testng.annotations.Test;

public class GetMethodForSingleUser {

	@Test

	public void singleUserGetMethod() {

		baseURI = "https://reqres.in";
		given().get("/api/users/2").then().body("data.last_name", equalTo("Weaver"));
	}

	@Test

	public void printValue() {
		baseURI = "https://reqres.in";
		given().get("/api/users/2").then().log().all();

	}

}
