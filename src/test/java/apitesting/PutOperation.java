package apitesting;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class PutOperation {

	@Test

	public void updatingDataUsingPut() {

		JSONObject request = new JSONObject();
		request.put("name", "Ramesh Babu");
		request.put("job", "Electrical Engineer");
		// System.out.println(request);

		String responce = given().body(request.toJSONString()).put("https://reqres.in/api/users/2").then()
				.statusCode(200).body("updatedAt", greaterThanOrEqualTo("2023-01-24T09:56:26.606Z")).log().all().toString();

		System.out.println(responce);
	}

}
