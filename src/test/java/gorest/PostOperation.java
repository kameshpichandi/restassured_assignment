package gorest;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;

public class PostOperation {
	
	public String token = "3f0951d65284088ef927e1b6689a2521f932f5a613fa866ddb710d39a2777700";
	public String authToken = "Bearer " + token;
	public String uri="https://gorest.co.in";

	@Test(enabled = false)
	public void creatUser() {
		baseURI = uri;
		JSONObject object = new JSONObject();
		object.put("name", "RameshBabu");
		object.put("email", "Ramesh94@gmail.com");
		object.put("gender", "male");
		object.put("status", "active");

		
		RequestSpecification requestSpecfication = RestAssured.given().header("Authorization", authToken)
				.contentType("application/json").body(object.toJSONString());

		Response response = requestSpecfication.post("/public/v2/users");
		int statusCode = response.getStatusCode();
		System.out.println("Status code is" +statusCode);
		Assert.assertEquals(201, statusCode);
		String payloadData = response.body().prettyPrint();
		System.out.println(payloadData);
		response.then().body("name", equalTo("RameshBabu"));

	}
	@Test(priority = 1)
	public void getUser() {
		baseURI = uri;
		
		RequestSpecification requestSpecfication = RestAssured.given().header("Authorization", authToken)
				.contentType("application/json");

		Response response = requestSpecfication.get("/public/v2/users/178147");
		int statusCode = response.getStatusCode();
		System.out.println("Status code is "+ statusCode);
		Assert.assertEquals(200, statusCode);
		String data = response.body().prettyPrint();
		System.out.println(data);
		response.then().body("email", equalTo("Ramesh94@gmail.com"));

	}
	
	
	@Test(priority = 2)
	
	public void updateUser() {
		baseURI = uri;
		JSONObject object = new JSONObject();
		object.put("name", "RaghuRam");
		object.put("email", "RaghuRam123@gmail.com");
		object.put("gender", "male");
		object.put("status", "active");
		
		RequestSpecification requestSpecfication = RestAssured.given().header("Authorization", authToken)
				.contentType("application/json").body(object.toJSONString());
		
		Response response = requestSpecfication.put("/public/v2/users/178147");
		int statusCode = response.getStatusCode();
		System.out.println("Status Code is :"+statusCode);
		Assert.assertEquals(200, statusCode);
		response.then().body("name", equalTo("RaghuRam"));
		String data = response.body().prettyPrint();
		System.out.println(data);

		
		
	}
	@Test(priority = 4)
	
	public void deletUser() {
		baseURI = uri;
		RequestSpecification requestSpecfication = RestAssured.given().header("Authorization", authToken)
				.contentType("application/json");
		Response response = requestSpecfication.delete("/public/v2/users/178147");
		int statusCode = response.statusCode();
		System.out.println("Status code " +statusCode);
		Assert.assertEquals(204, statusCode);
		
		
	}

}
